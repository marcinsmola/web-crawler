package com.smola.datagetters;


import com.smola.datagetters.otodom.OtodomAuctionDetailsCrawlerImpl;
import com.smola.model.HtmlDocument;
import com.smola.model.Price;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class HtmlAuctionDetailsCrawlerTest {
    private HtmlAuctionDetailsCrawler parser = new OtodomAuctionDetailsCrawlerImpl();
    private HtmlDocument validHtmlDocument;

    @Test
    public void shouldRetrieveCorrectNumberOfAuctions_fromHTMLDocument() throws IOException {
        //given
        String destination = "src/main/resources/websites/otodom-flats-list.html";
        File listOfAuctionsHTML = new File(destination);
        Document doc = Jsoup.parse(listOfAuctionsHTML, "UTF-8", "http://example.com/");
        validHtmlDocument = new HtmlDocument(doc, destination);

        //when
        List<String> actualFlats = parser.getAuctionsUrls(validHtmlDocument);

        //then
        assertNotNull(actualFlats);
        assertEquals(27, actualFlats.size());
    }

    @Test
    public void shouldRetrieveLinksToAuctions_fromHTMLDocument() throws IOException {
        //given
        String crawledLink = "https://www.otodom" +
                ".pl/oferta/mieszkanie-ogrodek-bezczynszowe-wola-mrokowska-mdm-ID2M257.html" +
                "#bbeb493d40";
        String destination = "src/main/resources/websites/otodom-flats-list.html";
        File listOfAuctionsHTML = new File(destination);
        Document doc = Jsoup.parse(listOfAuctionsHTML, "UTF-8", "http://example.com/");
        validHtmlDocument = new HtmlDocument(doc, destination);

        //when
        List<String> actualFlats = parser.getAuctionsUrls(validHtmlDocument);

        //then
        assertTrue(actualFlats.contains(crawledLink));
    }



    @Test
    public void shouldRetrieveAuctionTitle_fromHTMLDocument() throws IOException {
        //given
        String destination = "src/main/resources/websites/otodom-sample-auction.html";
        File sampleAuction = new File(destination);
        Document doc = Jsoup.parse(sampleAuction, "UTF-8", "http://example.com/");
        validHtmlDocument = new HtmlDocument(doc, destination);

        String expectedTitle = "Mieszkanie na starówce w bloku, 2 min od ratusza";
        String crawledTitle = parser.getAuctionTitle(validHtmlDocument);

        assertEquals(expectedTitle, crawledTitle);
    }

    @Test
    public void shouldRetrievePrice_fromHTMLDocument() throws IOException {
        String destination = "src/main/resources/websites/otodom-sample-auction.html";
        File sampleAuction = new File(destination);
        Document doc = Jsoup.parse(sampleAuction, "UTF-8", "http://example.com/");
        validHtmlDocument = new HtmlDocument(doc, destination);

        Price expectedPrice = Price.valueOf("279000");
        Price crawledPrice = parser.getAuctionPrice(validHtmlDocument);

        assertEquals(expectedPrice.getValue(), crawledPrice.getValue());
    }

    @Test
    public void shouldRetrieveFlatDescription_fromHTMLDocument() throws IOException {
        String destination = "src/main/resources/websites/otodom-sample-auction.html";
        File sampleAuction = new File(destination);
        Document doc = Jsoup.parse(sampleAuction, "UTF-8", "http://example.com/");
        validHtmlDocument = new HtmlDocument(doc, destination);

        String partOfExpectedDescription = "Przedmiotem sprzedaży jest mieszkanie położone w samym sercu płockiej " +
                "starówki." +
                " Mieszkanie znajduje " +
                "się w stosunkowo nowym budynku (nie starszym niż kilkanaście lat), oddalonym o dwie minuty " +
                "spacerkiem od ratusza.";


        String crawledDescription = parser.getAuctionDescription(validHtmlDocument);

        assertTrue(crawledDescription.contains(partOfExpectedDescription));
    }

    @Test
    public void shouldRetrieveFlatSize_fromHTMLDocument() throws IOException {
        String destination = "src/main/resources/websites/otodom-sample-auction.html";
        File sampleAuction = new File(destination);
        Document doc = Jsoup.parse(sampleAuction, "UTF-8", "http://example.com/");
        validHtmlDocument = new HtmlDocument(doc, destination);
        Integer expectedFlatSize = 72;

        Integer crawledFlatSize = parser.getFlatSize(validHtmlDocument);
        assertEquals(expectedFlatSize, crawledFlatSize);
    }

    @Test
    public void shouldRetrieveFlatLocation_fromHTMLDocument() throws IOException {
        String destination = "src/main/resources/websites/otodom-sample-auction.html";
        File sampleAuction = new File(destination);
        Document doc = Jsoup.parse(sampleAuction, "UTF-8", "http://example.com/");
        validHtmlDocument = new HtmlDocument(doc, destination);

        String expectedLocation = "Płock";
        String crawledLocation = parser.getLocation(validHtmlDocument);
        assertEquals(expectedLocation, crawledLocation);
    }

    @Test
    public void shouldRetrieveCountOfRooms_fromHTMLDocument() throws IOException {
        String destination = "src/main/resources/websites/otodom-sample-auction.html";
        File sampleAuction = new File(destination);
        Document doc = Jsoup.parse(sampleAuction, "UTF-8", "http://example.com/");
        validHtmlDocument = new HtmlDocument(doc, destination);
        Integer expectedRooms = 3;
        Integer actualRoomsNb = parser.getCountOfRooms(validHtmlDocument);
        assertEquals(expectedRooms, actualRoomsNb);
    }


}
