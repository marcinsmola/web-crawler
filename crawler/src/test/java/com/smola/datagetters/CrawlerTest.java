package com.smola.datagetters;

import com.smola.datagetters.otodom.OtodomAuctionDetailsCrawlerImpl;
import com.smola.model.Flat;
import com.smola.model.HtmlDocument;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CrawlerTest {
    private List<HtmlDocument> documentsToParse = new ArrayList<>();
    private HtmlAuctionDetailsCrawler crawler = new OtodomAuctionDetailsCrawlerImpl();
    private SingleAuctionParser singleAuctionParser = new SingleAuctionParser(crawler);
    private Crawler otodomCrawler = new Crawler(singleAuctionParser);

    @BeforeAll
    void setUp() throws IOException {
        String firstDestination = "src/main/resources/websites/otodom-sample-auction.html";
        String secondDestination = "src/main/resources/websites/otodom-sample-auction2.html";
        File firstAuction = new File(firstDestination);
        File secondAuction = new File(secondDestination);
        Document firstDoc = Jsoup.parse(firstAuction, "UTF-8", "http://example.com/");
        Document secondDoc = Jsoup.parse(secondAuction, "UTF-8", "http://example.com/");
        documentsToParse.add(new HtmlDocument(firstDoc, firstDestination));
        documentsToParse.add(new HtmlDocument(secondDoc, secondDestination));
    }

    @Test
    public void shouldRetrieveInformationsFromMultipleAuctions() {
        List<Flat> flats = otodomCrawler.getFlatsData(documentsToParse);
        assertTrue(flats.size() == 2);
    }
}
