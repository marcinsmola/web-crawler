package com.smola.datagetters;

import com.smola.datagetters.otodom.OtodomAuctionDetailsCrawlerImpl;
import com.smola.model.Flat;
import com.smola.model.FlatDetails;
import com.smola.model.HtmlDocument;
import com.smola.model.Price;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import static org.assertj.core.api.Assertions.assertThat;
import java.io.File;
import java.io.IOException;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SingleAuctionParserTest {
    private static final Price EXPECTED_PRICE = Price.valueOf("279000");
    private static final String EXPECTED_TITLE = "Mieszkanie na starówce w bloku, 2 min od ratusza";
    private static final String EXPECTED_DESCRIPTION = "Przedmiotem sprzedaży jest mieszkanie położone w samym sercu płockiej starówki. " +
                    "Mieszkanie znajduje się w stosunkowo nowym budynku (nie starszym niż kilkanaście lat), oddalonym" +
                    " o dwie " +
                    "minuty spacerkiem od ratusza. Mieszkanie znajduje się na trzecim czyli ostatnim piętrze budynku," +
                    " stąd " +
                    "obecność skosów, w tej samej klatce znajduje się zaledwie 6 lokali, w tym jeden to kancelaria " +
                    "adwokacka," +
                    " a drugi gabinet terapii naturalnej. Jak zatem sami Państwo widzą mamy do czynienia z wyjątkowo " +
                    "spokojną" +
                    " lokalizacją. Mieszkanie składa się z 6 pomieszczeń: olbrzymiego salonu, osobnej kuchni, " +
                    "przestronnego " +
                    "holu, dwóch mniejszych pokoi, w tym jednego z balkonem oraz łazienki. Mieszkanie po świeżo " +
                    "wykonanym " +
                    "malowaniu. Stan wykończenia lokalu pozwala na błyskawiczne zamieszkanie. Mieszkanie ze względu " +
                    "na " +
                    "lokalizację jest idealne dla osób, które lubią być blisko centrum wydarzeń, a jednocześnie " +
                    "ceniących " +
                    "spokój. Ze względu na swoją lokalizację mieszkanie może stanowić także doskonałą inwestycję jako" +
                    " " +
                    "apartament na wynajem krótkoterminowy lub po prostu lokatę kapitału. Na uwagę zasługuje także " +
                    "niezwykle " +
                    "niski czynsz wynoszący 257 zł, suma opłat miesięcznych wraz z funduszem remontowym wynosi " +
                    "dokładnie 327 " +
                    "zł. Powierzchnia rzeczywista mieszkania wynosi 72 m kw., ze względu na obecność \"skosów\" " +
                    "powierzchnia " +
                    "użytkowa mieszkania wynosi 54,13 m kw. Jestem właścicielem mieszkania, stąd decydując się na " +
                    "wybór mojej" +
                    " oferty zarówno Państwo jak i ja pominiemy opłaty pośredników. Jednocześnie wszystkich agentów " +
                    "proszę o " +
                    "uszanowanie mojego czasu i niekontaktowanie się ze mną. Osoby zainteresowane zakupem mieszkania " +
                    "serdecznie zapraszam do kontaktu telefonicznego, z przyjemnością odpowiem na wszystkie pytania i" +
                    " " +
                    "zaproszę na oględziny. ;)";

    private static final int EXPECTED_COUNT_OF_ROOMS = 3;
    private static final int EXPECTED_FLAT_SIZE = 72;
    private static final String EXPECTED_LOCATION = "Płock";
    private static final String EXPECTEED_DESTINATION = "src/main/resources/websites/otodom-sample-auction.html";

    private SingleAuctionParser singleAuctionParser;
    private HtmlAuctionDetailsCrawler otoDomCrawler;
    private FlatDetails EXPECTED_FLAT_DETAILS;
    private Flat EXPECTED_FLAT;

    @BeforeAll
    void setUp() {
        otoDomCrawler = new OtodomAuctionDetailsCrawlerImpl();
        singleAuctionParser = new SingleAuctionParser(otoDomCrawler);
        EXPECTED_FLAT_DETAILS = FlatDetails.builder()
                .flatDescription(EXPECTED_DESCRIPTION)
                .price(EXPECTED_PRICE)
                .title(EXPECTED_TITLE)
                .countOfRooms(EXPECTED_COUNT_OF_ROOMS)
                .flatSize(EXPECTED_FLAT_SIZE)
                .location(EXPECTED_LOCATION)
                .build();
        EXPECTED_FLAT = new Flat(EXPECTEED_DESTINATION,EXPECTED_FLAT_DETAILS);
    }

    @Test
    public void shouldGetAllNeccessaryInformationFromSingleAuction() throws IOException {
        String destination = "src/main/resources/websites/otodom-sample-auction.html";
        File sampleAuction = new File(destination);
        Document doc = Jsoup.parse(sampleAuction, "UTF-8", "http://example.com/");
        HtmlDocument htmlDocument = new HtmlDocument(doc, destination);
        Flat flatWithInformation = singleAuctionParser.crawl(htmlDocument);

        assertThat(flatWithInformation)
                .isEqualToComparingFieldByFieldRecursively(EXPECTED_FLAT);

    }

}
