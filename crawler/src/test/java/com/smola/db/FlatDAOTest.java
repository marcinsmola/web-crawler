package com.smola.db;

import com.smola.model.Flat;
import com.smola.model.FlatDetails;
import com.smola.model.Price;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class FlatDAOTest {
    private SetupInitializer dbSetup = new EmbededMongoSetup();
    private FlatDAO dao = new FlatDAO(dbSetup);

    @BeforeAll
    void setUp(){
        dbSetup.initialize();
    }
    @AfterEach
    void tearDown() {
        dao.removeAll();
    }

    @Test
    public void shouldSaveFlat_InDatabase() {
        //given
        FlatDetails flatDetails = FlatDetails.builder()
                .title("Some title")
                .price(Price.valueOf("2000"))
                .location("Some location")
                .build();
        Flat flatToSave = new Flat("Some auction url", flatDetails);

        //when
        dao.save(flatToSave);

        //then
        assertEquals(1, dao.getAll().size());
    }
    @Test
    public void shouldClearWholeCollection() {
        //given
        FlatDetails flatDetails = FlatDetails.builder()
                .title("Some title")
                .price(Price.valueOf("2000"))
                .location("Some location")
                .build();
        Flat flatToSave = new Flat("Some auction url", flatDetails);
        dao.save(flatToSave);

        //when
        dao.removeAll();

        //then
        assertEquals(0,dao.getAll().size());
    }
}