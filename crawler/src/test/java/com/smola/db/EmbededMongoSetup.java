package com.smola.db;

import com.mongodb.*;
import de.flapdoodle.embed.mongo.*;
import de.flapdoodle.embed.mongo.config.*;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import java.io.IOException;
import java.net.UnknownHostException;

class EmbededMongoSetup implements SetupInitializer {
    private static final MongodStarter starter = MongodStarter.getDefaultInstance();
    public static final String HOST_NAME = "localhost";
    public static final String DB_NAME = "xd";
    public static final int PORT = 12345;

    private MongodExecutable _mongodExe;
    private MongodProcess _mongod;
    private MongoClient _mongo;
    private Morphia morphia;
    private Datastore datastore;

    public void initialize() {
        try {
            _mongodExe = starter.prepare(new MongodConfigBuilder()
                    .version(Version.Main.PRODUCTION)
                    .net(new Net(HOST_NAME, PORT, Network.localhostIsIPv6()))
                    .build());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            _mongod = _mongodExe.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        _mongo = new MongoClient(HOST_NAME, PORT);
        morphia = new Morphia();
        morphia.mapPackage("com.smola");
        datastore = morphia.createDatastore(_mongo, DB_NAME);
        datastore.ensureIndexes();
    }

    @Override
    public Datastore getDatastore() {
        return datastore;
    }

}