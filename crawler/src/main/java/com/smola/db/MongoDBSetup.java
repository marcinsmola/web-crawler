package com.smola.db;

import com.mongodb.MongoClient;
import com.smola.ConfigLoader;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

public class MongoDBSetup implements SetupInitializer{
    private static final String HOST_NAME = ConfigLoader.getProperty("MONGO_HOST");
    private static final String HOST_PORT = ConfigLoader.getProperty("MONGO_PORT");
    private static final String DB_NAME = ConfigLoader.getProperty("MONGO_DB_NANE");
    private static final String PACKAGE_TO_MAP = ConfigLoader.getProperty("MONGO_PACKAGE_TO_MAP");
    private Morphia morphia;
    private Datastore datastore;
    {
        initialize();
    }

    public void initialize() {
        morphia = new Morphia();
        morphia.mapPackage(PACKAGE_TO_MAP);
        datastore = morphia.createDatastore(new MongoClient(HOST_NAME, Integer.parseInt(HOST_PORT)), DB_NAME);
        datastore.ensureIndexes();
    }

    public Datastore getDatastore() {
        return datastore;
    }
}
