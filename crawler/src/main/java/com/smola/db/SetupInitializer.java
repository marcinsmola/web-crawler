package com.smola.db;

import org.mongodb.morphia.Datastore;

public interface SetupInitializer {
    void initialize();
    Datastore getDatastore();
}
