package com.smola.db;

import java.util.Collection;

public interface DAO<T> {
    void save(T toSave);
    void save(Collection<T> toSave);
    Collection<T> getAll();
    void removeAll();
}
