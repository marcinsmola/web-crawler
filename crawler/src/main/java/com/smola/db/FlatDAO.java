package com.smola.db;

import com.smola.model.Flat;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;

import java.util.Collection;

public class FlatDAO implements DAO<Flat> {
    private SetupInitializer dbSetup;

    public FlatDAO(SetupInitializer dbSetup) {
        this.dbSetup = dbSetup;
    }

    @Override
    public void save(Flat toSave)
    {
        Datastore datastore = dbSetup.getDatastore();
        datastore.save(toSave);
    }

    @Override
    public void save(Collection<Flat> toSave) {
        for (Flat flat : toSave) {
            save(flat);
        }
    }

    @Override
    public Collection<Flat> getAll() {
        Datastore datastore = dbSetup.getDatastore();
        Query<Flat> query = datastore.createQuery(Flat.class);
        Collection<Flat> list = query.asList();
        return list;
    }

    @Override
    public void removeAll() {
        Datastore datastore = dbSetup.getDatastore();
        Query<Flat> query = datastore.createQuery(Flat.class);
        datastore.delete(query);
    }
}
