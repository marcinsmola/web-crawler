package com.smola;

public class App {
    public static void main(String[] args) {
        Facade facade = new Facade();
        facade.run();
    }
}
