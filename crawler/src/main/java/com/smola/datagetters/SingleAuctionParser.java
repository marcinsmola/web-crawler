package com.smola.datagetters;

import com.smola.model.Flat;
import com.smola.model.FlatDetails;
import com.smola.model.HtmlDocument;

import java.util.ArrayList;
import java.util.List;


public class SingleAuctionParser {
    private HtmlAuctionDetailsCrawler crawler;

    public SingleAuctionParser(HtmlAuctionDetailsCrawler crawler) {
        this.crawler = crawler;
    }

    public Flat crawl(HtmlDocument htmlDocument) {
        FlatDetails flatDetails = FlatDetails.builder()
                .title(crawler.getAuctionTitle(htmlDocument))
                .price(crawler.getAuctionPrice(htmlDocument))
                .flatDescription(crawler.getAuctionDescription(htmlDocument))
                .countOfRooms(crawler.getCountOfRooms(htmlDocument))
                .flatSize(crawler.getFlatSize(htmlDocument))
                .location(crawler.getLocation(htmlDocument))
                .build();

        return new Flat(htmlDocument.getUrl(),flatDetails);
    }

    public List<Flat> crawl(List<HtmlDocument> singleAuctions) {
        List<Flat> flats = new ArrayList<>();
        for (HtmlDocument singleAuction : singleAuctions) {
            flats.add(crawl(singleAuction));
        }
        return flats;
    }
}
