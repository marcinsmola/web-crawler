package com.smola.datagetters.otodom;

import com.smola.datagetters.HtmlAuctionDetailsCrawler;
import com.smola.model.HtmlDocument;
import com.smola.model.Price;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class OtodomAuctionDetailsCrawlerImpl implements HtmlAuctionDetailsCrawler {
    final static Logger logger = Logger.getLogger(String.valueOf(OtodomAuctionDetailsCrawlerImpl.class));

    @Override
    public List<String> getAuctionsUrls(HtmlDocument htmlDocument) {
        Document toParse = htmlDocument.getDoc();
        List<String> links = new ArrayList<>();
        Elements linksElements = toParse.getElementsByClass("offer-item-header").select("a[href]");
        for (Element element : linksElements) {
            String absouluteLink = element.attr("href");
            logger.info("Getting URL to crawl: " + absouluteLink);
            links.add(absouluteLink);
        }
        return links;
    }

    @Override
    public String getAuctionTitle(HtmlDocument htmlDocument) {
        Document toParse = htmlDocument.getDoc();
        Element firstElement = toParse.getElementsByClass("col-md-offer-content")
                .select("h1[itemprop=name]")
                .first();
        String title = firstElement.text();
        logger.info("Getting title from single auction. ");
        return title;
    }

    @Override
    public Price getAuctionPrice(HtmlDocument htmlDocument) {
        Document toParse = htmlDocument.getDoc();
        Element priceElement = toParse.getElementsByClass("box-price-value")
                .select("strong[itemprop=value]").first();
        String priceWithCurrency = priceElement.text();
        String priceValue = priceWithCurrency.replaceAll("[^0-9]", "");
        logger.info("Getting price from single auction");

        return Price.valueOf(priceValue);
    }

    @Override
    public String getAuctionDescription(HtmlDocument htmlDocument) {
        Document toParse = htmlDocument.getDoc();
        Element desctiptionElement = toParse.getElementsByClass("text-contents")
                .select("div[itemprop=description]").first();
        logger.info("Getting description from single auction.");
        return desctiptionElement.text();
    }

    @Override
    public Integer getFlatSize(HtmlDocument htmlDocument) {
        Document toParse = htmlDocument.getDoc();
        String flatSizeStringWithUnit = toParse.getElementsByClass("area-lane")
                .select(".big").text();
        String flatSizeValue = flatSizeStringWithUnit.replaceAll("[^0-9]", "");
        logger.info("Getting flat size.");
        return Integer.valueOf(flatSizeValue);
    }

    @Override
    public String getLocation(HtmlDocument htmlDocument) {
        Document toParse = htmlDocument.getDoc();
        String location = toParse.getElementsByClass("percentile-location-text").text();
        return location;
    }

    @Override
    public Integer getCountOfRooms(HtmlDocument htmlDocument) {
        Document toParse = htmlDocument.getDoc();
        String countOfRoomsElement = toParse.getElementsByClass("room-lane")
                .text()
                .replaceAll("[^0-9]", "");
        return Integer.valueOf(countOfRoomsElement);
    }


}
