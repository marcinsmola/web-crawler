package com.smola.datagetters;

import com.smola.model.HtmlDocument;

import java.util.List;
import java.util.Optional;

public interface DocumentGetter {
    HtmlDocument connect(String destination);

    List<HtmlDocument> connect(List<String> urls);
}
