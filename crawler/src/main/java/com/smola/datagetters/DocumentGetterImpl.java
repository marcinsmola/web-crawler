package com.smola.datagetters;

import com.smola.model.HtmlDocument;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DocumentGetterImpl implements DocumentGetter {

    private final Logger log = Logger.getLogger(getClass().getName());

    public HtmlDocument connect(String destination) {
        Document jsoupDocument = null;
        try {
            log.log(Level.INFO, "Connecting to: " + destination);
            jsoupDocument = Jsoup.connect(destination).get();
        } catch (IOException e) {
            log.log(Level.WARNING, e.getMessage());
        }
        return new HtmlDocument(jsoupDocument, destination);
    }

    @Override
    public List<HtmlDocument> connect(List<String> urls) {
        List<HtmlDocument> documents = new ArrayList<>();
        for (String url : urls) {
            HtmlDocument singleDocument = connect(url);
            documents.add(singleDocument);
        }
        return documents;
    }

}
