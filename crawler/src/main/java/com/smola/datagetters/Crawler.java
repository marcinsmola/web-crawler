package com.smola.datagetters;

import com.smola.model.Flat;
import com.smola.model.HtmlDocument;

import java.util.ArrayList;
import java.util.List;

public class Crawler {

    private final SingleAuctionParser singleAuctionParser;

    public Crawler(SingleAuctionParser singleAuctionParser) {
        this.singleAuctionParser = singleAuctionParser;
    }

    public List<Flat> getFlatsData(List<HtmlDocument> documentsToParse) {
        List<Flat> flats = new ArrayList<>();
        for (HtmlDocument document : documentsToParse) {
            flats.add(singleAuctionParser.crawl(document));
        }
        return flats;
    }
}
