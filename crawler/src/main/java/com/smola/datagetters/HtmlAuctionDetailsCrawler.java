package com.smola.datagetters;

import com.smola.model.HtmlDocument;
import com.smola.model.Price;

import java.util.List;
import java.util.Optional;

public interface HtmlAuctionDetailsCrawler {
    List<String> getAuctionsUrls(HtmlDocument htmlDocument);

    String getAuctionTitle(HtmlDocument htmlDocument);

    Price getAuctionPrice(HtmlDocument htmlDocument);

    String getAuctionDescription(HtmlDocument htmlDocument);

    Integer getFlatSize(HtmlDocument htmlDocument);

    String getLocation(HtmlDocument htmlDocument);

    Integer getCountOfRooms(HtmlDocument htmlDocument);

}
