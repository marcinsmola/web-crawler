package com.smola;

import com.smola.datagetters.*;
import com.smola.datagetters.otodom.OtodomAuctionDetailsCrawlerImpl;
import com.smola.db.DAO;
import com.smola.db.MongoDBSetup;
import com.smola.db.FlatDAO;
import com.smola.model.Flat;
import com.smola.model.HtmlDocument;

import java.util.List;

public class Facade {
    private DocumentGetter documentGetter = new DocumentGetterImpl();
    private HtmlAuctionDetailsCrawler crawler = new OtodomAuctionDetailsCrawlerImpl();
    private SingleAuctionParser singleAuctionParser = new SingleAuctionParser(crawler);
    private MongoDBSetup dbSetup = new MongoDBSetup();
    private DAO<Flat> dao = new FlatDAO(dbSetup);

    public void run() {
        dbSetup.initialize();
        String startingUrl = ConfigLoader.getProperty("OTODOM_STARTING_URL");
        HtmlDocument auctionsSite = documentGetter.connect(startingUrl);
        List<String> auctionsUrls = crawler.getAuctionsUrls(auctionsSite);
        List<HtmlDocument> singleAuctions = documentGetter.connect(auctionsUrls);
        List<Flat> finalFlats = singleAuctionParser.crawl(singleAuctions);
        dao.save(finalFlats);
    }
}
