package com.smola;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigLoader {
    private static final String CONFIG_FILE_NAME = "/config/config.properties";
    private static Properties properties = new Properties();

    static {
        loadProperties();
    }

    private static void loadProperties() {
        try {
            InputStream inputStream = ConfigLoader.class.getResourceAsStream(CONFIG_FILE_NAME);
            properties.load(inputStream);
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public static String getProperty(String key) {
        return properties.getProperty(key);
    }
}
