package com.smola.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@EqualsAndHashCode
@Getter
public class Price {
    private final double value;

    private Price() {
        this.value = 0;
    }

    private  Price(double value) {
        this.value = value;
    }

    public static Price valueOf(String string){
        return new Price(Double.valueOf(string));
    }



}