package com.smola.model;


import lombok.*;

@EqualsAndHashCode
@ToString
@Builder
@AllArgsConstructor
public final class FlatDetails {
    private final String location;
    private final String title;
    private final String flatDescription;
    private final Price price;
    private final int countOfRooms;
    private final int flatSize;

    private FlatDetails(){
        this.price = null;
        this.countOfRooms = 0;
        this.flatSize = 0;
        location = null;
        title = null;
        flatDescription = null;
    }

}
