package com.smola.model;


import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.mongodb.morphia.annotations.Id;

@EqualsAndHashCode
@ToString
public final class Flat {
    @Id
    private String id;
    private final String linkToAuction;
    private final FlatDetails flatDetails;

    private Flat() {
        this.linkToAuction = null;
        this.flatDetails = null;
    }

    public Flat(String linkToAuction, FlatDetails flatDetails) {
        this.linkToAuction = linkToAuction;
        this.flatDetails = flatDetails;
    }
}
