package com.smola.model;

import lombok.Getter;
import lombok.ToString;
import org.jsoup.nodes.Document;

@ToString
@Getter
public class HtmlDocument {
    private Document doc;
    private String url;

    public HtmlDocument(Document doc, String url) {
        this.doc = doc;
        this.url = url;
    }
}
